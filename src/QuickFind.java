/**
 * Created by mackwell on 17/03/2016.
 */
public class QuickFind {

    private static int[] id;

    public QuickFind(int n) {
        id = new int[n];
        for (int i = 0; i < n; i++) {
            id[i] = i;
        }

    }

    public boolean find(int p, int q) {
        return id[p] == id[q];
    }

    public void union(int p, int q) {
        int pid = id[p];
        for (int i = 0; i < id.length; i++) {
            if (id[i] == pid) {
                id[i] = id[q];
            }
        }
    }

    public static void main(String[] args) {
        QuickFind quickFind = new QuickFind(10);
        quickFind.union(9,0);
        quickFind.union(5,6);
        quickFind.union(2,1);
        quickFind.union(2,9);
        quickFind.union(7,9);
        quickFind.union(1,5);


        for (int i = 0; i< id.length; i++) {
            System.out.print(id[i] +  " ");
        }
    }
}
