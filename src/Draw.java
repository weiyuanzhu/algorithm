/**
 * Created by mackwell on 15/03/2016.
 */


import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

public class Draw {

    public static void main(String[] args) {
        int N = 50;

//        StdDraw.setXscale(0,N);
//        StdDraw.setYscale(0,N * N);
//        StdDraw.setPenRadius(.01);
//
//        for (int i = 1; i < N; i++) {
//            StdDraw.point(i,i);
//            StdDraw.point(i, i*i);
//            StdDraw.point(i,i*Math.log(i));
//        }
//
//        StdDraw.filledCircle(50.0,50.0,10.0);
//        StdDraw.filledRectangle(1.0/N, 10.0/2.0, 0.5/N, 10/2.0);

        double[] a = new double[N];

        for (int i=0; i<N; i++) {
            a[i] = StdRandom.uniform(0,1.0);
//            StdOut.println(a[i]);
        }

        for (int i = 0; i < N; i++) {
            double x = 1.0*i/N;
            double y = a[i] / 2.0;
            double rw = 0.5/N;
            double rh = a[i] / 2.0;

            StdDraw.filledRectangle(x,y,rw,rh);
        }

        //StdDraw.circle(0.5,0.5,0.5);

    }
}
