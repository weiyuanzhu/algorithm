import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

/**
 * Created by mackwell on 15/03/2016.
 */
public class BinarySearch {

    public static int rank(int key, int[] array, int low, int high) {
        int mid;
        int lo = low;
        int hi = high;

        if (high - low == 0) {
            if (array[lo] == key) {
                return lo;
            } else {
                return -1;
            }
        } else if (high - low == 1 ) {
            if (array[low] == key) {
                return low;
            } else if (array[high] == key) {
                return high;
            } else return -1;

        } else {
                mid = lo + (hi - lo) /2;

                if (array[mid] > key) {
                    hi = mid - 1;
                  return rank(key, array,lo, hi);

                } else if (array[mid] < key) {
                    lo = mid + 1;
                    return rank(key, array,lo,hi);

                } else return mid;
            }
        }

    public static int rank2(int key, int[] array) {

        int lo = 0;
        int hi = array.length -1;

        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;

            if (array[mid] > key) {
                hi = mid - 1;
            } else if (array[mid] < key) {
                lo = mid + 1;
            } else {
                return mid;
            }


        }

        return -1;
    }

    public static void main(String[] args) {
        //int[] test = {1,4,6,7,9,11,34,56,78,97,100};
        int[] whiteList = new In(args[0]).readAllInts();
        Arrays.sort(whiteList);

        //int key = 77;

        //StdOut.println (rank2(key, test));

        int tCount = 0;

        while (!StdIn.isEmpty()) {
            tCount++;
            int key = StdIn.readInt();
//            if (rank(key, whiteList,0,(whiteList.length -1 )) < 0) {
//                StdOut.println(key);
//            }

            if (rank2(key, whiteList) < 0) {
                StdOut.println(key);
            }
        }

        StdOut.println("WhiteList length: " + whiteList.length);
        StdOut.println("TList length: " + tCount);
    }

}
