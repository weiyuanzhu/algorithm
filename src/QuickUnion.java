/**
 * Created by mackwell on 17/03/2016.
 */
public class QuickUnion {
    static private int[] id;
    static private int[] size;

    public QuickUnion(int n) {
        id = new int[n];
        size = new int[n];
        for (int i = 0; i < n; i++) {
            id[i] = i;
            size[i] = 1;
        }
    }

    public int root(int p) {

        while (p != id[p]) {
            //id[p] = id[id[p]];
            p = id[p];
        }

        return p;
    }

    public void union(int p, int q) {

        id[p] = root(q);
    }

    public boolean connected(int p, int q) {

        return root(p) == root(q);
    }

    public void weightedUnion(int p, int q) {
        int rootP = root(p);
        int rootQ = root(q);

        if (rootP == rootQ) return;

        if (size[rootP] < size[rootQ]) {
            id[rootP] = id[rootQ];
            size[rootQ] += size[rootP];
        } else {
            id[rootQ] = id[rootP];
            size[rootP] += size[rootQ];
        }
    }

    public static void main(String[] args) {


        QuickUnion qu = new QuickUnion(10);
        qu.weightedUnion(0,5);
        qu.weightedUnion(3,4);
        qu.weightedUnion(4,7);
        qu.weightedUnion(4,9);
        qu.weightedUnion(6,2);
        qu.weightedUnion(8,9);
        qu.weightedUnion(5,6);
        qu.weightedUnion(9,6);
        qu.weightedUnion(1,2);




        for (int i = 0; i< id.length; i++) {
            System.out.print(id[i] +  " ");
        }


    }

}
