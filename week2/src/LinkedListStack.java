import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;


/**
 * Created by mackwell on 30/03/2016.
 */
public class LinkedListStack {

     class Node {
        private String item;
        private Node next;
    }

    private  Node first = null;

    public  boolean isEmpty() {
        return first == null;

    }

    public  String pop() {
        String item = first.item;
        first = first.next;
        return item;

    }

    public  void push(String item) {
        Node oldFirst = first;
        first = new Node();
        first.item = item;
        first.next = oldFirst;

    }

    public static void main(String[] args) {
        LinkedListStack stack = new LinkedListStack();
        while (!StdIn.isEmpty()) {
            String in = StdIn.readString();
            if (in.equals("-")) {
                StdOut.println(stack.pop());
            } else {
                stack.push(in);
            }

        }



    }



}
