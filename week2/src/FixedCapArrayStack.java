/**
 * Created by mackwell on 30/03/2016.
 */
public class FixedCapArrayStack {

    private String[] stack;
    private int N = 0;

    public FixedCapArrayStack(int cap) {
        stack = new String[cap];
    }

    public boolean isEmpty() {
        return N == 0;
    }

    public void push(String item) {
        stack[N++] = item;
    }

    public String pop() {
        String first = stack[--N];
        stack[N] = null;
        return first;
    }

    public static void main(String[] args) {
        FixedCapArrayStack stack = new FixedCapArrayStack(10);
        stack.push("to");
        stack.push("be");

        System.out.println(stack.pop());
        System.out.println(stack.pop());

    }

}
