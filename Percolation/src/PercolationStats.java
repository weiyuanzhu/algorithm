import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

/**
 * Created by mackwell on 18/03/2016.
 */

public class PercolationStats {

    private int T;

    private double mean;
    private double std;
    private double confidenceLo;
    private double confidenceHi;

    private double[] thresholds;

    private Percolation perc;



    public PercolationStats(int N, int T) {
        if (N <= 0 || T <= 0) {
            throw new java.lang.IllegalArgumentException("N T should be > 0 ");
        } else {
            this.T = T;

            thresholds = new double[T];




            for (int i = 0; i < T; i++) {
                perc = new Percolation(N);
                int openCount = 0;

                while (!perc.percolates()) {
                    int p = StdRandom.uniform(1, N + 1);
                    int q = StdRandom.uniform(1, N + 1);

                    if (!perc.isOpen(p, q)) {
                        perc.open(p, q);
                        openCount++;
                    }

                }


                thresholds[i] =  openCount / (double) (N * N);
            }

            StdOut.println(mean());
            StdOut.println(stddev());
            StdOut.println(confidenceLo() + ", " + confidenceHi());


        }

    }

    public double mean() {
        this.mean = StdStats.mean(thresholds);
        return this.mean;
    }

    public double stddev() {
        this.std = StdStats.stddev(thresholds);
        return this.std;
    }

    public double confidenceLo() {
        this.confidenceLo = this.mean - 1.96 * this.std / Math.sqrt(this.T);
        return this.confidenceLo;
    }

    public double confidenceHi() {
        this.confidenceHi = this.mean + 1.96 * this.std / Math.sqrt(this.T);
        return this.confidenceHi;
    }

    public static void main(String[] args) {
//
//        for (int i=0; i < 10; i++) {
//            StdOut.println(StdRandom.uniform(1,2));
//        }
        PercolationStats stats;

        if (args.length == 2) {
            stats = new PercolationStats(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        }




    }



}
