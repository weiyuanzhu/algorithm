/**
 * Created by mackwell on 18/03/2016.
 */

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private WeightedQuickUnionUF wQUUF;
    private WeightedQuickUnionUF wQUUF2;  // find-union object without virtual bottom



    private int N = 0;
    private int length = 0;
    private boolean[] openList;

    public Percolation(int N) {
        if (N <= 0) {
            throw new java.lang.IllegalArgumentException("N must be greater than 0");
        } else {
            this.N = N;
            this.length = N * N + 2;
            openList = new boolean[length];

            wQUUF = new WeightedQuickUnionUF(length);
            wQUUF2 = new WeightedQuickUnionUF(length - 1);

            //connect top row

            for (int i = 0; i <= N * N; i++) {
                openList[i] = false;
            }

        }
   }

    public void open(int i, int j) {

        if (validPoint(i,j)) {

            int index = getIndex(i, j);
            if (!isOpen(i, j)) {
                if(i == 1) {
                    wQUUF.union(0, index);
                    wQUUF2.union(0, index);
                }
                if (i == N) {
                    wQUUF.union(N * N + 1, index);
                }

                openList[index] = true;
                connectToNeighbour(i, j);
            }

        } else {
            throw new java.lang.IndexOutOfBoundsException("Index out of bounds");
        }

    }

    public boolean isOpen(int i, int j) {

        if (validPoint(i, j)) {
            int index = getIndex(i, j);
            return openList[index];
        } else {
            throw new java.lang.IndexOutOfBoundsException("Index out of bounds");
        }

    }

    public boolean isFull(int i, int j) {

        if (validPoint(i, j)) {
            int index = getIndex(i, j);

            boolean open = isOpen(i, j);

            return (wQUUF2.connected(index, 0) && open);

        } else {
            throw new java.lang.IndexOutOfBoundsException("Index out of bounds");
        }

    }

    public boolean percolates() {

        return  wQUUF.connected(0, length -1);
    }

    private int getIndex(int i, int j) {
        return ((i - 1) * N + j);
    }

    private void connectToNeighbour(int i, int j) {

        if (validPoint(i - 1, j) && isOpen(i - 1, j) && !wQUUF2.connected(getIndex(i, j), getIndex(i - 1, j))) {
            wQUUF.union(getIndex(i, j), getIndex(i - 1, j));
            wQUUF2.union(getIndex(i, j), getIndex(i - 1, j));
        }
        if (validPoint(i + 1, j) && isOpen(i + 1, j) && !wQUUF2.connected(getIndex(i, j), getIndex(i + 1, j))) {
            wQUUF.union(getIndex(i, j), getIndex(i + 1, j));
            wQUUF2.union(getIndex(i, j), getIndex(i + 1, j));
        }
        if (validPoint(i, j - 1) && isOpen(i, j - 1) && !wQUUF2.connected(getIndex(i, j), getIndex(i, j - 1))) {
            wQUUF.union(getIndex(i, j), getIndex(i, j - 1));
            wQUUF2.union(getIndex(i, j), getIndex(i, j - 1));
        }
        if (validPoint(i, j + 1) && isOpen(i, j + 1) && !wQUUF2.connected(getIndex(i, j), getIndex(i, j + 1))) {
            wQUUF.union(getIndex(i, j), getIndex(i, j + 1));
            wQUUF2.union(getIndex(i, j), getIndex(i, j + 1));
        }
    }

    private boolean validPoint(int i, int j) {
        boolean result = (i > 0 && i <= N && j > 0 && j <= N);
        return result;
    }

}
